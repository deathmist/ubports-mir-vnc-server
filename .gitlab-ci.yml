---
variables:
  DOCKER_DRIVER: overlay2

###############################################################################
# Define all YAML node anchors
###############################################################################
.node_anchors:
  # `stage`
  stage_build: &stage_build 'build'
  stage_lint: &stage_lint 'lint'
  # `image`
  image_black: &image_black
    name: 'cytopia/black:latest'
    entrypoint: ['/bin/ash', '-c']
  image_clickable_amd64: &image_clickable_amd64 'clickable/ci-16.04-amd64'
  image_clickable_arm64: &image_clickable_arm64 'clickable/ci-16.04-arm64'
  image_clickable_armhf: &image_clickable_armhf 'clickable/ci-16.04-armhf'
  image_shellcheck: &image_shellcheck 'koalaman/shellcheck-alpine:latest'
  image_yamllint: &image_yamllint
    name: 'cytopia/yamllint:latest'
    entrypoint: ['/bin/ash', '-c']
  # `artifacts.paths`
  paths_amd64: &paths_amd64
    - 'build/x86_64-linux-gnu/app/*.click'
  paths_arm64: &paths_arm64
    - 'build/aarch64-linux-gnu/app/*.click'
  paths_armhf: &paths_armhf
    - 'build/arm-linux-gnueabihf/app/*.click'

###############################################################################
# Define stages
###############################################################################
stages:
  - *stage_lint
  - *stage_build

###############################################################################
# `lint` stage: `yamllint`, `black` & `shellcheck`
###############################################################################
yamllint:
  stage: *stage_lint
  image: *image_yamllint
  script:
    - 'yamllint -s .'

black:
  stage: *stage_lint
  image: *image_black
  script:
    # The `--diff` will show any files that need to be fixed
    # The `--check` will provide the failure, if encountered
    - 'black --diff .'
    - 'black --check -v .'

shellcheck:
  stage: *stage_lint
  image: *image_shellcheck
  script:
    - 'shellcheck --version'
    - 'find . -type f \( -iname \*.sh -o -iname \*.bash -o -iname \*.ksh \)
                      | xargs shellcheck'

###############################################################################
# Define `build` template
###############################################################################
.build_click:
  stage: *stage_build
  script:
    - 'clickable build'
    - 'clickable review'
  artifacts:
    expire_in: '1 week'

###############################################################################
# `build` stage: build clicks for `armhf`, `arm64` & `amd64`
###############################################################################
xenial_armhf_click:
  extends: '.build_click'
  image: *image_clickable_armhf
  artifacts:
    paths: *paths_armhf

xenial_arm64_click:
  extends: '.build_click'
  image: *image_clickable_arm64
  artifacts:
    paths: *paths_arm64

xenial_amd64_click:
  extends: '.build_click'
  image: *image_clickable_amd64
  artifacts:
    paths: *paths_amd64
