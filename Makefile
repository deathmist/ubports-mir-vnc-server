
BINS=mirvncserver

all: $(BINS)

MIRLIBS=-lboost_program_options -lpthread -lmirclient -lEGL -lxcb-glx -lGLESv2 -lmirserver -lmircore -levdev
QTLIBS=-lstdc++ -lQt5Gui -lQt5Core
VNCLIBS=-lvncserver

MIRCFLAGS=-std=c++11 -Wall -fpermissive -I/usr/include/mirclient -I/usr/include/mircommon -I/usr/include/mircore -I/usr/include/libevdev-1.0
QTCFLAGS=-I/usr/include/$(MULTIARCH)/qt5 -fPIC

mirvncserver.o: mirvncserver.cpp
	$(CXX) mirvncserver.cpp -c $(MIRCFLAGS) $(QTCFLAGS)

mirvncserver: mirvncserver.o
	$(CXX) mirvncserver.o -o mirvncserver $(MIRLIBS) $(VNCLIBS) $(QTLIBS)

clean:
	rm $(BINS) *.o
