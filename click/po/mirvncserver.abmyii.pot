# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the mirvncserver.abmyii package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: mirvncserver.abmyii\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-05-25 21:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:136
msgid "Menu"
msgstr ""

#: ../qml/Main.qml:182 ../qml/ui/SettingsPage.qml:11
msgid "Settings"
msgstr ""

#: ../qml/Main.qml:183 ../qml/ui/AboutPage.qml:10
msgid "About"
msgstr ""

#: ../qml/components/aboutpage/ExternalDialog.qml:11
msgid "Open external link"
msgstr ""

#: ../qml/components/aboutpage/ExternalDialog.qml:26
msgid "You are about to open an external link. Do you wish to continue?"
msgstr ""

#: ../qml/components/aboutpage/IconComponent.qml:33
msgid "Version"
msgstr ""

#: ../qml/components/aboutpage/IconComponent.qml:41
msgid "Released under license"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:17
msgid "Custom"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:18
#: ../qml/components/listmodels/SettingsModels.qml:34
msgid "Quarter"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:19
#: ../qml/components/listmodels/SettingsModels.qml:33
msgid "Third"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:20
#: ../qml/components/listmodels/SettingsModels.qml:32
msgid "Half"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:21
#: ../qml/components/listmodels/SettingsModels.qml:31
msgid "Native"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:56
msgid "Built-in Display"
msgstr ""

#: ../qml/components/listmodels/SettingsModels.qml:58
msgid "Display %1"
msgstr ""

#: ../qml/components/mainpage/RotationButton.qml:86
msgid "Aa"
msgstr ""

#: ../qml/components/mainpage/RotationButton.qml:86
msgid "Au"
msgstr ""

#: ../qml/components/settingspage/CustomPrecision.qml:13
msgid "Custom max precision"
msgstr ""

#: ../qml/components/settingspage/CustomPrecision.qml:77
msgid "i.e."
msgstr ""

#: ../qml/ui/AboutPage.qml:72
msgid "Report a bug"
msgstr ""

#: ../qml/ui/AboutPage.qml:72 ../qml/ui/AboutPage.qml:73
#: ../qml/ui/AboutPage.qml:74 ../qml/ui/AboutPage.qml:75
#: ../qml/ui/AboutPage.qml:76 ../qml/ui/AboutPage.qml:77
#: ../qml/ui/AboutPage.qml:78
msgid "Support"
msgstr ""

#: ../qml/ui/AboutPage.qml:73
msgid "Contact Developer"
msgstr ""

#: ../qml/ui/AboutPage.qml:74
msgid "View source"
msgstr ""

#: ../qml/ui/AboutPage.qml:75
msgid "Donate to Kugi"
msgstr ""

#: ../qml/ui/AboutPage.qml:76
msgid "View in OpenStore"
msgstr ""

#: ../qml/ui/AboutPage.qml:77
msgid "Other apps by Abdurrahmaan"
msgstr ""

#: ../qml/ui/AboutPage.qml:78
msgid "Other apps by Kugi"
msgstr ""

#: ../qml/ui/AboutPage.qml:79 ../qml/ui/AboutPage.qml:80
msgid "Developers"
msgstr ""

#: ../qml/ui/AboutPage.qml:79
msgid "Main Developer"
msgstr ""

#: ../qml/ui/AboutPage.qml:80
msgid "Developer"
msgstr ""

#: ../qml/ui/BottomEdgeHint.qml:35
msgid ""
"Swipe from either side of the bottom edge to activate the corresponding "
"actions in the header. \n"
" Try it now!"
msgstr ""

#: ../qml/ui/BottomEdgeHint.qml:44
msgid "Okay"
msgstr ""

#: ../qml/ui/MainPage.qml:139
msgid "Hide Log"
msgstr ""

#: ../qml/ui/MainPage.qml:139
msgid "Show Log"
msgstr ""

#: ../qml/ui/MainPage.qml:151
msgid "Clear Log"
msgstr ""

#: ../qml/ui/MainPage.qml:163
msgid "View Connections"
msgstr ""

#: ../qml/ui/MainPage.qml:295
msgid "Starting..."
msgstr ""

#: ../qml/ui/MainPage.qml:296
msgid "Running: "
msgstr ""

#: ../qml/ui/MainPage.qml:297
msgid "Not Started"
msgstr ""

#: ../qml/ui/MainPage.qml:297
msgid "Not Started: Error"
msgstr ""

#: ../qml/ui/MainPage.qml:308 ../qml/ui/MainPage.qml:309
msgid "@%1Hz"
msgstr ""

#: ../qml/ui/MainPage.qml:308
msgid "Native: "
msgstr ""

#: ../qml/ui/MainPage.qml:309
msgid "Cast: "
msgstr ""

#: ../qml/ui/MainPage.qml:310
msgid "Rotate (Experimental): "
msgstr ""

#: ../qml/ui/MainPage.qml:327
msgid "Log"
msgstr ""

#: ../qml/ui/MainPage.qml:362
msgid "No log entries..."
msgstr ""

#: ../qml/ui/MainPage.qml:402 ../qml/ui/SettingsPage.qml:52
msgid ""
"App suspension needs to be disabled for the VNC server to work in the "
"background"
msgstr ""

#: ../qml/ui/MainPage.qml:413 ../qml/ui/SettingsPage.qml:59
msgid "Set"
msgstr ""

#: ../qml/ui/MainPage.qml:425
msgid "No"
msgstr ""

#: ../qml/ui/MainPage.qml:440
msgid "Start Server"
msgstr ""

#: ../qml/ui/MainPage.qml:440
msgid "Stop Server"
msgstr ""

#: ../qml/ui/SettingsPage.qml:51
msgid "App is already set to prevent suspension"
msgstr ""

#: ../qml/ui/SettingsPage.qml:59
msgid "Unset"
msgstr ""

#: ../qml/ui/SettingsPage.qml:65
msgid "Start server at app startup"
msgstr ""

#: ../qml/ui/SettingsPage.qml:81
msgid "Always show log"
msgstr ""

#: ../qml/ui/SettingsPage.qml:97
msgid "(Relative to native orientation)"
msgstr ""

#: ../qml/ui/SettingsPage.qml:97
msgid "Automatic rotation"
msgstr ""

#: ../qml/ui/SettingsPage.qml:113
msgid "(Reduces battery discharge)"
msgstr ""

#: ../qml/ui/SettingsPage.qml:113
msgid "Turn off backlight when starting the server"
msgstr ""

#: ../qml/ui/SettingsPage.qml:129
msgid "View only (No input sent to server)"
msgstr ""

#: ../qml/ui/SettingsPage.qml:147
msgid "Resolution"
msgstr ""

#: ../qml/ui/SettingsPage.qml:162
msgid "Custom Width"
msgstr ""

#: ../qml/ui/SettingsPage.qml:176
msgid "Custom Height"
msgstr ""

#: ../qml/ui/SettingsPage.qml:188
msgid "Refresh Rate"
msgstr ""
