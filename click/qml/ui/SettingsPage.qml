import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import "../components/common"
import "../components/settingspage"

BasePage {
    id: settingsPage
    
    title: i18n.tr("Settings")
    flickable: settingsFlickable
    
    
    Flickable {
        id: settingsFlickable
        
        anchors.fill: parent
        contentHeight: settingsColumn.implicitHeight + (settingsColumn.anchors.margins * 2)
        boundsBehavior: Flickable.DragOverBounds

        ScrollIndicator.vertical: ScrollIndicator { }
        
        function moveScroll(newY){
            contentY = newY
        }
    
        Column {
            id: settingsColumn
            
            spacing: 10
            
            anchors{
                fill: parent
                margins: 25
            }

            RowLayout {
                visible: gsettings.lifecycleExemptAppids ?  true : false
                anchors {
                    left: parent.left
                    right: parent.right
                }

                Label {
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignVCenter
                    Suru.highlightType: mainView.appSuspensionDisabled ? Suru.PositiveHighlight : Suru.NegativeHighlight
                    color: Suru.highlightColor
                    wrapMode: Text.WordWrap
                    text: mainView.appSuspensionDisabled ? i18n.tr("App is already set to prevent suspension")
                            : i18n.tr("App suspension needs to be disabled for the VNC server to work in the background")
                }

                Button {
                    Layout.preferredWidth: Suru.units.gu(10)
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                    
                    text: mainView.appSuspensionDisabled ? i18n.tr("Unset") : i18n.tr("Set")
                    onClicked: mainView.appSuspensionDisabled ? mainView.unsetAppSuspension() : mainView.setAppSuspension()
                }
            }

            CheckBoxItem {
                text: i18n.tr("Start server at app startup")

                anchors {
                    left: parent.left
                    right: parent.right
                }

                Component.onCompleted: {
                    checked = settings.startonStartup
                }
                onCheckedChanged: {
                    settings.startonStartup = checked
                }
            }

            CheckBoxItem {
                text: i18n.tr("Always show log")

                anchors {
                    left: parent.left
                    right: parent.right
                }

                Component.onCompleted: {
                    checked = settings.alwaysShowLog
                }
                onCheckedChanged: {
                    settings.alwaysShowLog = checked
                }
            }

            CheckBoxItem {
                text: i18n.tr("Automatic rotation") + "\n" + i18n.tr("(Relative to native orientation)")

                anchors {
                    left: parent.left
                    right: parent.right
                }

                Component.onCompleted: {
                    checked = settings.autoRotation
                }
                onCheckedChanged: {
                    settings.autoRotation = checked
                }
            }
            
            CheckBoxItem {
                text: i18n.tr("Turn off backlight when starting the server") + "\n" + i18n.tr("(Reduces battery discharge)")

                anchors {
                    left: parent.left
                    right: parent.right
                }

                Component.onCompleted: {
                    checked = settings.disableBacklight
                }
                onCheckedChanged: {
                    settings.disableBacklight = checked
                }
            }

            CheckBoxItem {
                text: i18n.tr("View only (No input sent to server)")

                anchors {
                    left: parent.left
                    right: parent.right
                }

                Component.onCompleted: {
                    checked = settings.viewOnly
                }
                onCheckedChanged: {
                    settings.viewOnly = checked
                }
            }

            ComboBoxItem {       
                id: themeSettings
                        
                text: i18n.tr("Resolution")
                model: settingsModels.resolutionModel
                currentIndex: model.find(settings.resolutionFraction, "code")
                textRole: "text"
                
                onCurrentIndexChanged:{
                    settings.resolutionFraction = model.get(currentIndex).code
                }
            }
            
            TextFieldItem {
                id: customWidthText

                anchors.leftMargin: Suru.units.gu(1)
                visible: settings.resolutionFraction == 0
                title: i18n.tr("Custom Width")
                placeholderText: "1920"
                inputMethodHints: Qt.ImhDigitsOnly
                validator:IntValidator{}
                value: settings.customWidth
                
                onValueChanged: settings.customWidth = value
            }
            
            TextFieldItem {
                id: customHeightText

                anchors.leftMargin: Suru.units.gu(1)
                visible: settings.resolutionFraction == 0
                title: i18n.tr("Custom Height")
                placeholderText: "1080"
                inputMethodHints: Qt.ImhDigitsOnly
                validator:IntValidator{}
                value: settings.customHeight
                
                onValueChanged: settings.customHeight = value
            }

            ComboBoxItem {       
                id: refreshRateSettings
                        
                text: i18n.tr("Refresh Rate")
                model: settingsModels.refreshRateModel
                currentIndex: model.find(settings.refreshRateFraction, "code")
                textRole: "text"
                
                onCurrentIndexChanged:{
                    settings.refreshRateFraction = model.get(currentIndex).code
                }
            }
        }
    }
}
