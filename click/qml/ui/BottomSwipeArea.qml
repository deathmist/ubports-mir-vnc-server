import Ubuntu.Components 1.3
import QtQuick.Controls.Suru 2.2

SwipeArea{
	id: bottomSwipeArea
	
	signal triggered
	
	direction: SwipeArea.Upwards
	height: Suru.units.gu(2)
	z: 1
	anchors.bottom: parent.bottom
	
	onDraggingChanged: {
		if(dragging){
			triggered()
		}	
	}
}
