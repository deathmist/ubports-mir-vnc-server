import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as UITK

Drawer {
    id: menuDrawer
    
    property alias model: listView.model
    
    readonly property real minimumWidth: Suru.units.gu(20)
    readonly property real maximumWidth: Suru.units.gu(40)
    readonly property real preferredWidth: mainView.width * 0.25

    width: Math.min(maximumWidth , Math.max(preferredWidth, minimumWidth))
    height: mainView.height
    interactive: stackView.depth === 1
    dragMargin: 0
    
    background: Rectangle{color: Suru.backgroundColor}
    
    function openTop(){
        listView.verticalLayoutDirection = ListView.TopToBottom
        open()
    }
    
    function openBottom(){
        listView.verticalLayoutDirection = ListView.BottomToTop
        open()
    }
    
    function resetListIndex(){
        listView.currentIndex = -1
    }

    ListView {
        id: listView

        focus: true
        currentIndex: -1
        anchors.fill: parent
        anchors.topMargin: applicationHeader.expanded ? applicationHeader.height - applicationHeader.defaultHeight : 0

        delegate: ItemDelegate {
            width: parent.width
            text: modelData.title
            highlighted: ListView.isCurrentItem
            
            indicator: UITK.Icon {
                 id: iconMenu
                 
                 implicitWidth: Suru.units.gu(3)
                 implicitHeight: implicitWidth
                 anchors.left: parent.left
                 anchors.leftMargin: Suru.units.gu(1)
                 anchors.verticalCenter: parent.verticalCenter
                 name: modelData ? modelData.iconName : ""
                 color: Suru.foregroundColor
             }
             leftPadding: iconMenu.implicitWidth + (iconMenu.anchors.leftMargin * 2)
            
            onClicked: {
                listView.currentIndex = index
                stackView.push(modelData.source)
                drawer.close()
            }
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }
}
