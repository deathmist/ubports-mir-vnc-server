import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.4 //for Python
import GSettings 1.0
import "../components/common"
import "../components/mainpage"

BasePage {
    id: mainPage
    
    readonly property real devicePixelRatio: settingsModels.screensModel.get(displayId - 1).data.devicePixelRatio
    
    readonly property int angleOrientation: Screen.angleBetween(Screen.primaryOrientation, Screen.orientation)
    readonly property int screenHeight: Screen.height * devicePixelRatio
    readonly property int screenWidth: Screen.width * devicePixelRatio
    readonly property int deviceHeight: angleOrientation == 0 || angleOrientation == 180 ? screenHeight : screenWidth
    readonly property int deviceWidth: angleOrientation == 0 || angleOrientation == 180 ? screenWidth : screenHeight
    readonly property int castWidth: settings.resolutionFraction > 0 ? (deviceWidth / settings.resolutionFraction)
                                                                            : settings.customWidth
    readonly property int castHeight: settings.resolutionFraction > 0 ? (deviceHeight / settings.resolutionFraction)
                                                                            : settings.customHeight
    property int displayId: displaysComboBox.currentIndex + 1
    property int castRotation: rotationButton.rotation

    property bool showLog: false
    property bool serverRunning: false
    property bool serverStarting: false
    property bool isError: false
    property var vnc_process
    property int originalBacklightBrightness
    property bool backlightDisabled: false
    
    flickable: mainFlickable
    
    // Avoid binding loop warning
    implicitWidth: parent.width

    title: "MirVNCServer"

    headerRightActions: [logAction, clearLogAction, connectionsAction]

    function disableBacklight() {
        backlightDisabled = true
        py.call('vncserver.getBacklight', [], function (result) {
            originalBacklightBrightness = result
        });
        py.call('vncserver.setBacklight', [0], function () {});
    }

    function enableBacklight() {
        backlightDisabled = false
        py.call('vncserver.setBacklight', [originalBacklightBrightness], function () {});
    }
    
    function startServer() {
        isError = false
        serverStarting = true
        
        var viewOnly = settings.viewOnly ? "--view-only" : ""

        var finalWidth = screenNativeOrientation ?
                    displayId > 1 || screenNativeOrientation == Screen.orientation || (screenNativeOrientation / Screen.orientation) == 4 
                    || (screenNativeOrientation / Screen.orientation) == 0.25 ? castWidth.toString() : castHeight.toString() : castWidth.toString()
        var finalHeight = screenNativeOrientation ?
                    displayId > 1 || screenNativeOrientation == Screen.orientation || (screenNativeOrientation / Screen.orientation) == 4 
                    || (screenNativeOrientation / Screen.orientation) == 0.25 ? castHeight.toString() : castWidth.toString() : castHeight.toString()
        
        py.call('vncserver.start', [displayId.toString(), viewOnly, finalWidth, finalHeight, settings.refreshRateFraction.toString(), castRotation.toString()], function (result) {
            mainPage.vnc_process = result
            checkStatus()
        });

        if (settings.disableBacklight) {
            disableBacklight()
        }
    }
    
    function stopServer() {
        py.call('vncserver.stop', [mainPage.vnc_process], function (result) {
            console.log("stop result: " + result)
            if (result) {
                mainPage.serverRunning = false

                if (backlightDisabled) {
                    enableBacklight()
                }
            }
            statusTimer.stop()
        });
    }
    
    function checkStatus() {
        py.call('vncserver.is_running', [mainPage.vnc_process], function (result) {
            if (result) {
                statusTimer.restart()
            } else {
                mainPage.isError = true
                mainPage.serverRunning = result
            }
        });
        py.call('vncserver.getBacklight', [], function (result) {
            if (backlightDisabled && result == originalBacklightBrightness) {
                backlightDisabled = false
            }
        });
    }
    
    function log(text) {
        if (outputLabel.text) {
            outputLabel.text = outputLabel.text + "\n" + text
        } else {
            outputLabel.text = text
        }
    }
    
    function clearLog() {
        outputLabel.text = ""
    }
    
    onServerRunningChanged: serverStarting = false
    onIsErrorChanged: serverStarting = false
    
    Timer {
        id: statusTimer

        interval: 500

        onTriggered: {
            mainPage.checkStatus()
        }
    }

    BaseAction {
        id: logAction

        text: mainPage.showLog ? i18n.tr("Hide Log") : i18n.tr("Show Log")
        iconName: mainPage.showLog ? "view-off" : "view-on"
        enabled: !settings.alwaysShowLog
    
        onTrigger:{
            mainPage.showLog = !mainPage.showLog
        }
    }
    
    BaseAction {
        id: clearLogAction

        text: i18n.tr("Clear Log")
        iconName: "edit-clear"
        enabled: mainPage.showLog || settings.alwaysShowLog
    
        onTrigger:{
            mainPage.clearLog()
        }
    }
    
    BaseAction {
        id: connectionsAction
    
        text: i18n.tr("View Connections")
        iconName: "nm-adhoc"
        enabled: false
    
        onTrigger:{
            console.log("Connections!")
        }
    }

    //initiate a python component for calls to python commands
    Python {
        id: py

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl("../py/"));
            importModule('vncserver', function () {
                    if (settings.startonStartup) {
                        startServer()
                    }
                });

            setHandler('running', function(serverStatus) {
                    console.log("serverStatus: " + serverStatus)
                    mainPage.serverRunning = serverStatus
                });

            setHandler('log', function(log) {
                    var logText = String(log)
                    console.log("Log: " + logText)
                    if (logText == "Running vnc background loop...\n") {
                        mainPage.serverRunning = true
                    }
                    mainPage.log(logText)
                });
        }

        onError: {
            mainPage.isError = true
            mainPage.log(traceback)
        }
    }
    
    Flickable {
        id: mainFlickable
        
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: bottomControls.top
        }
        contentHeight: mainColumnLayout.height + Suru.units.gu(5)
        boundsBehavior: Flickable.DragOverBounds
        clip: true
        
        ScrollIndicator.vertical: ScrollIndicator { }
    
        ColumnLayout {
            id: mainColumnLayout

            spacing: Suru.units.gu(2)

            anchors {
                margins: Suru.units.gu(2)
                top: parent.top
                left: parent.left
                right: parent.right
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignVCenter
                Layout.fillWidth: true
                spacing: Suru.units.gu(1)
                
                RowLayout {
                    Layout.preferredWidth: Suru.units.gu(20)
                    Layout.maximumWidth: Suru.units.gu(25)
                    Layout.alignment: Qt.AlignHCenter

                    ComboBox {
                        id: displaysComboBox

                        Layout.fillWidth: true
                        enabled: model.count > 1 && !mainPage.serverStarting && !mainPage.serverRunning
                        model: settingsModels.screensModel
                        textRole: "displayName"
                    }
                    
                    RotationButton {
                        id: rotationButton

                        Layout.preferredWidth: Suru.units.gu(5)
                        Layout.preferredHeight: Layout.preferredWidth
                        Layout.alignment: Qt.AlignRight || Qt.AlignVCenter

                        auto: settings.autoRotation && displayId == 1
                        nativeOrientation: screenNativeOrientation > 0 ? screenNativeOrientation : Screen.primaryOrientation
                        enabled: !mainPage.serverStarting && !mainPage.serverRunning && !auto
                        lockAuto: mainPage.serverStarting || mainPage.serverRunning
                    }

                    BacklightButton {
                        id: backlightButton

                        Layout.preferredWidth: Suru.units.gu(5)
                        Layout.preferredHeight: Layout.preferredWidth
                        Layout.alignment: Qt.AlignRight || Qt.AlignVCenter

                        enabled: mainPage.serverRunning
                        iconName: backlightDisabled ? "display-brightness-min" : "display-brightness-max"

                        onClicked: {
                            if (!backlightDisabled) {
                                disableBacklight()
                            } else {
                                enableBacklight()
                            }
                        }
                    }
                }

                Label {
                    id: statusLabel

                    Layout.alignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    Suru.highlightType: mainPage.serverRunning ? Suru.PositiveHighlight
                            : mainPage.isError ? Suru.NegativeHighlight : Suru.InformationHighlight
                    color: Suru.highlightColor
                    font: Suru.units.fontHeadingOne
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: mainPage.serverStarting ? i18n.tr("Starting...") : 
                            mainPage.serverRunning ? i18n.tr("Running: ") + ipAddress
                                : mainPage.isError ? i18n.tr("Not Started: Error") : i18n.tr("Not Started")
                    
                }

                Label {
                    id: currentSettings

                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: i18n.tr("Native: ") + mainPage.deviceWidth + "x" + mainPage.deviceHeight + i18n.tr("@%1Hz").arg(refreshRate)
                            + "\n" + i18n.tr("Cast: ") + mainPage.castWidth + "x" + mainPage.castHeight + i18n.tr("@%1Hz").arg((refreshRate / settings.refreshRateFraction).toFixed(2))
                            + "\n" + i18n.tr("Rotate (Experimental): ") + "<Ctrl>+<Super>+<Up/Down>"
                    
                    font: Suru.units.fontSmall
                    color: Suru.secondaryForegroundColor
                }
            }
                
            ColumnLayout {
                id: logRowLayout
                
                visible: mainPage.showLog || settings.alwaysShowLog
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignBottom
                
                Label {
                    Layout.fillWidth: true
                    Layout.margins: units.gu(1)
                    text: i18n.tr("Log")
                    font: Suru.units.fontHeadingThree
                }

                GroupBox {
                    id: logGroupbox
                    
                    Layout.fillWidth: true
                    Layout.margins: units.gu(1)
                    Layout.preferredHeight: outputLabel.height + Suru.units.gu(2)
                    Layout.maximumHeight: Suru.units.gu(40)
                    padding: 0

                    Flickable {
                        
                        clip: true
                        contentHeight: outputLabel.height
                        boundsBehavior: Flickable.StopAtBounds
                        ScrollBar.vertical: ScrollBar { }
                        anchors.fill: parent
                        contentY: contentHeight - height + (outputLabel.anchors.margins * 2)
                        
                        Label {
                            id: outputLabel

                            anchors {
                                left: parent.left
                                right: parent.right
                                top: parent.top
                                margins: Suru.units.gu(1)
                            }
                            font: Suru.units.fontCodeBlock
                            wrapMode: Text.Wrap
                
                            Label {
                                text: i18n.tr("No log entries...")
                                anchors {
                                    fill: parent
                                }
                                visible: !outputLabel.text
                                font: Suru.units.fontCodeBlock
                                horizontalAlignment: Text.AlignLeft
                                verticalAlignment: Text.AlignVCenter
                            }
                        }
                    }
                }
            }
        }
    }
    
    ColumnLayout {
        id: bottomControls
        
        spacing: Suru.units.gu(1)
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Suru.units.gu(2)
        }

        RowLayout {
            id: warningRow

            visible: settings.askSuspension && gsettings.lifecycleExemptAppids && !mainView.appSuspensionDisabled ?  true : false
            Layout.fillWidth: true
            Layout.topMargin: Suru.units.gu(2)

            Label {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignVCenter
                Suru.highlightType: mainView.appSuspensionDisabled ? Suru.PositiveHighlight : Suru.NegativeHighlight
                color: Suru.highlightColor
                wrapMode: Text.WordWrap
                text: i18n.tr("App suspension needs to be disabled for the VNC server to work in the background")
            }

            Button {
                Layout.preferredWidth: Suru.units.gu(7)
                Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                
                highlighted: true
                flat: true
                font.capitalization: Font.AllUppercase
                font.weight: Font.Normal
                text: i18n.tr("Set")
                onClicked: mainView.setAppSuspension()
            }

            Button {
                Layout.preferredWidth: Suru.units.gu(7)
                Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                
                highlighted: true
                flat: true
                font.capitalization: Font.AllUppercase
                font.weight: Font.Normal
                text: i18n.tr("No")
                onClicked: {
                    settings.askSuspension = false
                    warningRow.visible = false
                }
            }
        }

        Button {
            id: startButton

            Layout.fillWidth: true
            Layout.preferredHeight: Suru.units.gu(10)

            enabled: !mainPage.serverStarting
            text: mainPage.serverRunning || mainPage.serverStarting ? i18n.tr('Stop Server') : i18n.tr('Start Server')
            font: Suru.units.fontHeadingTwo
            Suru.highlightType: Suru.NegativeHighlight

            onClicked: {
                if (mainPage.serverRunning) {
                    mainPage.stopServer()
                } else {
                    mainPage.startServer()
                }
            }
        }
    }
}
