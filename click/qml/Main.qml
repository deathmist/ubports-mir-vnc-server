/*
 * Copyright (C) 2021  Abdurrahmaan Iqbal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * mirvncserver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls.Suru 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtSystemInfo 5.0
import GSettings 1.0
import "components/listmodels"
import "components/common"
import "ui"

ApplicationWindow {
    id: mainView

    readonly property QtObject drawer: drawerLoader.item
    readonly property string current_version: "1.2.2"
    property bool appSuspensionDisabled: false

    objectName: "mainView"

    title: "MirVNCServer"
    visible: false
    minimumWidth: Suru.units.gu(30)

    width: Suru.units.gu(45)
    height: Suru.units.gu(75)

    ScreenSaver {
        screenSaverEnabled: false
    }
    
    UITK.MainView{
        //Only for making translation work
        id: dummyMainView
        applicationName: "mirvncserver.abmyii"
        visible: false
    }

    function checkAppSuspension() {
        var appidList = gsettings.lifecycleExemptAppids
        var appId = Qt.application.name
        var found = false
        
        if (appidList) {
            for(var i = 0; i < appidList.length; i++){ 
                if (appidList[i] == appId) {
                    found = true
                }
            }
        }
        
        return found
    }

    function setAppSuspension() {
        if (!checkAppSuspension()) {
            var appidList = gsettings.lifecycleExemptAppids
            var appId = Qt.application.name
            
            var newList = appidList.slice()
            newList.push(appId)
            console.log("Original app suspension list: " + appidList)
            console.log("Set app suspension: " + newList)
            gsettings.lifecycleExemptAppids = newList
        }
    }

    function unsetAppSuspension() {
        if (checkAppSuspension()) {
            var appidList = gsettings.lifecycleExemptAppids
            var appId = Qt.application.name
            var index = appidList.indexOf(appId);

            console.log("Original app suspension list: " + appidList)

            if (index > -1) {
              appidList.splice(index, 1);
            }
            console.log("Unset app suspension: " + appidList)
            gsettings.lifecycleExemptAppids = appidList
            
        }
    }

    GSettings {
        id: gsettings
        schema.id: "com.canonical.qtmir"

        Component.onCompleted: {
            mainView.appSuspensionDisabled = mainView.checkAppSuspension()
        }

        onValueChanged: {
            if (key == "lifecycleExemptAppids") {
                mainView.appSuspensionDisabled = mainView.checkAppSuspension()
            }
        }
    }

    SettingsComponent{
        id: settings
    }

    Connections {
        id: keyboard
        target: Qt.inputMethod
    }
    
    SettingsModels{
        id: settingsModels
    }

    header: ApplicationHeader{
        id: applicationHeader
        
        flickable: stackView.currentItem.flickable
        leftActions: BaseAction{
            visible: drawerLoader.visible
            text: i18n.tr("Menu")
            iconName: stackView.depth > 1 ? "back" : "navigation-menu"
            
            onTrigger:{
                if (stackView.depth > 1) {
                        stackView.pop()
                        drawer.resetListIndex()
                    } else {
                        if(isBottom){
                            drawer.openBottom()
                        }else{
                            drawer.openTop()
                        }
                    }
                }
            }
            
        rightActions: stackView.currentItem ? stackView.currentItem.headerRightActions : 0
    }

    Loader {
        id: bottomEdgeHintLoader
        
        z: 10
        active: settings.firstRun
        asynchronous: true
        visible: status == Loader.Ready
        sourceComponent: BottomEdgeHint {
                onClose: settings.firstRun = false
            }
        
        anchors{
            fill: parent
            bottomMargin: Suru.units.gu(2)
        }
    }

    Loader {
        id: drawerLoader
        
        active: true
        asynchronous: true
        sourceComponent: MenuDrawer {
                id: drawer
                 
                 model:  [
                    { title: i18n.tr("Settings"), source: Qt.resolvedUrl("ui/SettingsPage.qml"), iconName: "settings" }
                    ,{ title: i18n.tr("About"), source: Qt.resolvedUrl("ui/AboutPage.qml"), iconName: "info" }
                ]
            }

        visible: status == Loader.Ready
    }
    
    StackView {
        id: stackView
        
        initialItem: Rectangle{color: Suru.backgroundColor}
        
        anchors{
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: keyboardRec.top
        }  
    }
    
    Loader {
        id: mainPageLoader
        
        active: true
        asynchronous: true
        source: "ui/MainPage.qml"

        visible: status == Loader.Ready

        onLoaded: {
            mainView.visible = true
            stackView.replace(item)
        }
    }  
    
    KeyboardRectangle{
        id: keyboardRec
    }
    
    Loader {
        id: rightSwipeAreaLoader
        
        active: true
        asynchronous: true
        visible: status == Loader.Ready
        sourceComponent: BottomSwipeArea{
            onTriggered: applicationHeader.triggerRight(true)
        }
        
        anchors{
            right: parent.right
            left: parent.horizontalCenter
            bottom: parent.bottom
        }
    }  
    
    Loader {
        id: leftSwipeAreaLoader
        
        active: true
        asynchronous: true
        visible: status == Loader.Ready
        sourceComponent: BottomSwipeArea{
            onTriggered: applicationHeader.triggerLeft(true)
        }
        
        anchors{
            left: parent.left
            right: parent.horizontalCenter
            bottom: parent.bottom
        }
    } 
}
