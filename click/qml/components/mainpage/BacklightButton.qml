import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as UITK
import QtSensors 5.2

Button {
	id: backlightButton

	focusPolicy: Qt.NoFocus
	flat: true

    property alias iconName: icon.name
    
	UITK.Icon {
		id: icon

        name: "display-brightness-max"
		implicitWidth: Suru.units.gu(4)
		implicitHeight: implicitWidth
		anchors.centerIn: parent
        color: Suru.highlightColor
        Suru.highlightType: backlightButton.auto ? Suru.PositiveHighlight : Suru.InformationHighlight
	}
}
