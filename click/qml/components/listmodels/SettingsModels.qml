import QtQuick 2.9
import QtQuick.Window 2.2

Item{
    id: settingsModels
    
    property alias resolutionModel: resolutionModel
    property alias refreshRateModel: refreshRateModel
    property alias screensModel: screensModel
    
    BaseListModel {
        id: resolutionModel
        
        Component.onCompleted: fillData()

        function fillData(){
            append({"text": i18n.tr("Custom"), "code": 0})
            append({"text": i18n.tr("Quarter") + " (" + Screen.width / 4  + "x" + Screen.height / 4  + ")", "code": 4})
            append({"text": i18n.tr("Third") + " (" + Screen.width / 3  + "x" + Screen.height / 3  + ")", "code": 3})
            append({"text": i18n.tr("Half") + " (" + Screen.width / 2  + "x" + Screen.height / 2  + ")", "code": 2})
            append({"text": i18n.tr("Native") + " (" + Screen.width  + "x" + Screen.height  + ")", "code": 1})
        }
    }

    BaseListModel {
        id: refreshRateModel
        
        Component.onCompleted: fillData()

        function fillData(){
            append({"text": i18n.tr("Native"), "code": 1})
            append({"text": i18n.tr("Half"), "code": 2})
            append({"text": i18n.tr("Third"), "code": 3})
            append({"text": i18n.tr("Quarter"), "code": 4})
        }
    }
    
    BaseListModel {
        id: screensModel
        
        readonly property var qtScreensModel: Qt.application.screens
        
        onQtScreensModelChanged: {
            clear()
            fillData()
        }

        function fillData(){
            var temp
            var displayName

            for (var i = 0; i < qtScreensModel.length; i++) {
                temp = qtScreensModel[i]
                
                if (i == 0) {
                    displayName = i18n.tr("Built-in Display")
                } else {
                    displayName = i18n.tr("Display %1").arg(i + 1)
                }
                append({"displayName": displayName, "id": i + 1, "data": temp})
            }
        }
    }
}
