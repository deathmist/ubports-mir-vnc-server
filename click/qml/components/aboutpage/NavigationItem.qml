import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3 as UITK
    
ItemDelegate {
    id: navigationItem
    
    property alias title: titleLabel.text
    property alias subtitle: subtitleLabel.text
    property alias iconName: icon.name
    property string url
    
    anchors{
        left: parent.left
        right: parent.right
    }
    
    height: Suru.units.gu(8)
    
    onClicked: flickable.externalLinkConfirmation(url)
    
    contentItem: RowLayout{
        UITK.Icon {
            id: icon
            
            Layout.preferredWidth: Suru.units.gu(3)
            Layout.preferredHeight: Layout.preferredWidth
            Layout.alignment: Qt.AlignVCenter
            visible: name
        }
        
        ColumnLayout {
            Layout.fillHeight: false
            Layout.alignment: Qt.AlignVCenter
            
            spacing: Suru.units.gu(1)
            Label {
                id: titleLabel
                
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignVCenter
                Layout.preferredHeight: font.pixelSize

                verticalAlignment: Qt.AlignVCenter
                elide: Text.ElideRight
                color: Suru.foregroundColor
                visible: text
            }
            
            Label {
                id: subtitleLabel
                
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignVCenter
                Layout.preferredHeight: font.pixelSize

                verticalAlignment: Qt.AlignVCenter
                font: Suru.units.fontCaption
                elide: Text.ElideRight
                color: Suru.foregroundColor
                visible: text
            }
        }

        UITK.Icon {
            Layout.preferredWidth: Suru.units.gu(3)
            Layout.preferredHeight: Layout.preferredWidth
            name: "go-next"
        }
    }
}

