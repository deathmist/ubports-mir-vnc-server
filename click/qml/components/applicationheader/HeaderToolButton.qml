import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as UITK

ToolButton {
    id: headerToolButton
    
    property alias iconName: icon.name
    
    anchors.bottom: parent.bottom

    contentItem: UITK.Icon {
        id: icon
        
        implicitWidth: Suru.units.gu(3)
        implicitHeight: implicitWidth
    }
}
