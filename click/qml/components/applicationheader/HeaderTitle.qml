import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2

Label {
    id: headerTitle

    font: Suru.units.fontHeadingTwo
    elide: Label.ElideRight
    fontSizeMode: Text.HorizontalFit
    minimumPixelSize: Suru.units.gu(2)
    horizontalAlignment: Text.AlignLeft
    verticalAlignment: Text.AlignVCenter
}
