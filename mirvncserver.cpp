/*
 * Copyright © 2014 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#include <QtGui/QImage>

#include <mirclient/mir_toolkit/mir_client_library.h>
#include <mirclient/mir_toolkit/mir_screencast.h>
#include <mircore/mir/geometry/size.h>
#include <mircore/mir/geometry/rectangle.h>

#include <mircommon/mir/raii.h>

#include <boost/program_options.hpp>

#include <getopt.h>
#include <csignal>

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <fstream>
#include <sstream>
#include <thread>
#include <iostream>
#include <stdexcept>
#include <future>
#include <vector>
#include <utility>
#include <chrono>

#include <unistd.h>
#include <rfb/rfb.h>
#include <rfb/keysym.h>

#include <stdio.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */


namespace po = boost::program_options;

volatile sig_atomic_t running = 1;

/* On devices with android based openGL drivers, the vendor dispatcher table
 * may be optimized if USE_FAST_TLS_KEY is set, which hardcodes a TLS slot where
 * the vendor opengl function pointers live. Since glibc is not aware of this
 * use, collisions may happen.
 * Allocating a thread_local array helps avoid collisions by any thread_local usage
 * in async/future implementations.
 */
thread_local int dummy_tls[2];

struct libevdev *dev;
struct libevdev_uinput *uidev;

int vnc_surface_width, vnc_surface_height;
bool landscape = false;
bool mirror = false;
float scale_factor;
int orientation;

bool lmb;
bool rmb;
bool mmb;
bool paused;
bool viewonly = false;

std::vector<bool> keys(0xFFFF, false);

uint32_t rot_left = 0xFF52;  // Ctrl+Super+Up
uint32_t rot_right = 0xFF54;  // Ctrl+Super+Down
uint32_t key_pause = 0x0020;  // Space


void shutdown(int) {
    running = 0;
}

void read_pixels(int bpp, GLenum format, mir::geometry::Size const& size, std::vector<char> &fb)
{
    auto width = size.width.as_uint32_t();
    auto height = size.height.as_uint32_t();

    // Read pixels into image (https://community.khronos.org/t/render-to-texture-under-qt/68430/3)
    QImage image(width, height, format == GL_BGRA_EXT ? QImage::Format_ARGB32 : QImage::Format_RGBA8888);
    glReadPixels(0, 0, width, height, format, GL_UNSIGNED_BYTE, image.bits());

    if (format == GL_BGRA_EXT)
        image = image.convertToFormat(QImage::Format_RGBA8888);

    if (mirror)
	image = image.mirrored(true, true);

    if (landscape) {
        QMatrix matrix;
        matrix.translate(0, 0);
        matrix.rotate(90);

        image = image.transformed(matrix);
    }

    std::memcpy(fb.data(), image.constBits(), image.byteCount());
}


uint32_t get_first_valid_output_id(MirDisplayConfiguration const& display_config)
{
    for (unsigned int i = 0; i < display_config.num_outputs; ++i)
    {
        MirDisplayOutput const& output = display_config.outputs[i];

        if (output.connected && output.used &&
            output.current_mode < output.num_modes)
        {
            return output.output_id;
        }
    }

    throw std::runtime_error("Couldn't find a valid output to screencast");
}

MirRectangle get_screen_region_from(MirDisplayConfiguration const& display_config, uint32_t output_id)
{
    if (output_id == mir_display_output_id_invalid)
        output_id = get_first_valid_output_id(display_config);

    for (unsigned int i = 0; i < display_config.num_outputs; ++i)
    {
        MirDisplayOutput const& output = display_config.outputs[i];

        if (output.output_id == output_id &&
            output.current_mode < output.num_modes)
        {
            MirDisplayMode const& mode = output.modes[output.current_mode];
            return MirRectangle{output.position_x, output.position_y,
                                mode.horizontal_resolution,
                                mode.vertical_resolution};
        }
    }

    throw std::runtime_error("Couldn't get screen region of specified output");
}

MirScreencastParameters get_screencast_params(MirConnection* connection,
                                              MirDisplayConfiguration const& display_config,
                                              std::vector<int> const& requested_size,
                                              std::vector<int> const& requested_region,
                                              uint32_t output_id)
{
    MirScreencastParameters params;

    if (requested_region.size() == 4)
    {
        /* TODO: the region should probably be validated
         * to make sure it overlaps any one of the active display areas
         */
        params.region.left = requested_region[0];
        params.region.top = requested_region[1];
        params.region.width = requested_region[2];
        params.region.height = requested_region[3];
    }
    else
    {
        params.region = get_screen_region_from(display_config, output_id);
    }

    if (requested_size.size() == 2)
    {
        params.width = requested_size[0];
        params.height = requested_size[1];
    }
    else
    {
        params.width = params.region.width;
        params.height = params.region.height;
    }

    unsigned int num_valid_formats = 0;
    mir_connection_get_available_surface_formats(connection, &params.pixel_format, 1, &num_valid_formats);

    if (num_valid_formats == 0)
        throw std::runtime_error("Couldn't find a valid pixel format for connection");

    return params;
}

double get_capture_rate_limit(MirDisplayConfiguration const& display_config, MirScreencastParameters const& params)
{
    mir::geometry::Rectangle screencast_area{
        {params.region.left, params.region.top},
        {params.region.width, params.region.height}};

    double highest_refresh_rate = 0.0;
    for (unsigned int i = 0; i < display_config.num_outputs; ++i)
    {
        MirDisplayOutput const& output = display_config.outputs[i];
        if (output.connected && output.used &&
            output.current_mode < output.num_modes)
        {
            MirDisplayMode const& mode = output.modes[output.current_mode];
            mir::geometry::Rectangle display_area{
                {output.position_x, output.position_y},
                {mode.horizontal_resolution, mode.vertical_resolution}};
            if (display_area.overlaps(screencast_area) && mode.refresh_rate > highest_refresh_rate)
                highest_refresh_rate = mode.refresh_rate;
        }
    }

    /* Either the display config has invalid refresh rate data
     * or the screencast region is outside of any display area, either way
     * let's default to sensible max capture limit
     */
    if (highest_refresh_rate == 0.0)
        highest_refresh_rate = 60.0;

    return highest_refresh_rate;
}

struct EGLSetup
{
    EGLSetup(MirConnection* connection, MirScreencast* screencast)
    {
        static EGLint const attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RED_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_BLUE_SIZE, 8,
            EGL_ALPHA_SIZE, 8,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_NONE};

        static EGLint const context_attribs[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE };

        auto native_display =
            reinterpret_cast<EGLNativeDisplayType>(
                mir_connection_get_egl_native_display(connection));

	MirBufferStream *buffer_stream_;
	buffer_stream_ = mir_screencast_get_buffer_stream(screencast);
        auto native_window =
            reinterpret_cast<EGLNativeWindowType>(
                mir_buffer_stream_get_egl_native_window(buffer_stream_)
	    );

        egl_display = eglGetDisplay(native_display);

        eglInitialize(egl_display, nullptr, nullptr);

        int n;
        eglChooseConfig(egl_display, attribs, &egl_config, 1, &n);

        egl_surface = eglCreateWindowSurface(egl_display, egl_config, native_window, nullptr);
        if (egl_surface == EGL_NO_SURFACE)
            throw std::runtime_error("Failed to create EGL screencast surface");

        egl_context = eglCreateContext(egl_display, egl_config, EGL_NO_CONTEXT, context_attribs);
        if (egl_context == EGL_NO_CONTEXT)
            throw std::runtime_error("Failed to create EGL context for screencast");

        if (eglMakeCurrent(egl_display, egl_surface,
                           egl_surface, egl_context) != EGL_TRUE)
        {
            throw std::runtime_error("Failed to make screencast surface current");
        }

	// glReadPixels fails with GL_INVALID_OPERATION (0x502 / 1282) on devices which don't support BGRA
        uint32_t a_pixel;
        glReadPixels(0, 0, 1, 1, GL_BGRA_EXT, GL_UNSIGNED_BYTE, &a_pixel);
        if (glGetError() == GL_NO_ERROR)
            read_pixel_format = GL_BGRA_EXT;
        else {
            read_pixel_format = GL_RGBA;
	}
    }

    ~EGLSetup()
    {
        eglMakeCurrent(egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(egl_display, egl_surface);
        eglDestroyContext(egl_display, egl_context);
        eglTerminate(egl_display);
    }

    void swap_buffers() const
    {
        if (eglSwapBuffers(egl_display, egl_surface) != EGL_TRUE)
            throw std::runtime_error("Failed to swap screencast surface buffers");
    }

    GLenum pixel_read_format() const
    {
        return read_pixel_format;
    }

    EGLDisplay egl_display;
    EGLContext egl_context;
    EGLSurface egl_surface;
    EGLConfig egl_config;
    GLenum read_pixel_format;
};


// https://github.com/hanzelpeter/dispmanx_vnc/blob/master/main.c
int keysym2scancode(rfbKeySym key)
{
	int scancode = 0;

	int code = (int) key;
	if (code>='0' && code<='9') {
		scancode = (code & 0xF) - 1;
		if (scancode<0) scancode += 10;
		scancode += KEY_1;
	} else if (code>=0xFF50 && code<=0xFF58) {
		static const uint16_t map[] =
		{  KEY_HOME, KEY_LEFT, KEY_UP, KEY_RIGHT, KEY_DOWN,
		KEY_PAGEUP, KEY_PAGEDOWN, KEY_END, 0 };
		scancode = map[code & 0xF];
	} else if (code>=0xFFE1 && code<=0xFFEE) {
		static const uint16_t map[] =
		{  KEY_LEFTSHIFT, KEY_LEFTSHIFT,
		KEY_RIGHTSHIFT, KEY_LEFTCTRL,
		KEY_RIGHTCTRL, KEY_LEFTSHIFT,
		0,0,
		KEY_LEFTALT, KEY_RIGHTALT,
		0, 0, 0, 0 };
		scancode = map[code & 0xF];
	} else if ((code>='A' && code<='Z') || (code>='a' && code<='z')) {
		static const uint16_t map[] = {
			KEY_A, KEY_B, KEY_C, KEY_D, KEY_E,
			KEY_F, KEY_G, KEY_H, KEY_I, KEY_J,
			KEY_K, KEY_L, KEY_M, KEY_N, KEY_O,
			KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T,
			KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z };
		scancode = map[(code & 0x5F) - 'A'];
	} else {
		switch (code) {
		case XK_space:    scancode = KEY_SPACE;       break;

		case XK_exclam: scancode = KEY_1; break;
		case XK_at:     scancode         = KEY_2; break;
		case XK_numbersign:    scancode      = KEY_3; break;
		case XK_dollar:    scancode  = KEY_4; break;
		case XK_percent:    scancode = KEY_5; break;
		case XK_asciicircum:    scancode     = KEY_6; break;
		case XK_ampersand:    scancode       = KEY_7; break;
		case XK_asterisk:    scancode        = KEY_8; break;
		case XK_parenleft:    scancode       = KEY_9; break;
		case XK_parenright:    scancode      = KEY_0; break;
		case XK_minus:    scancode   = KEY_MINUS; break;
		case XK_underscore:    scancode      = KEY_MINUS; break;
		case XK_equal:    scancode   = KEY_EQUAL; break;
		case XK_plus:    scancode    = KEY_EQUAL; break;
		case XK_BackSpace:    scancode       = KEY_BACKSPACE; break;
		case XK_Tab:    scancode             = KEY_TAB; break;

		case XK_braceleft:    scancode        = KEY_LEFTBRACE;     break;
		case XK_braceright:    scancode       = KEY_RIGHTBRACE;     break;
		case XK_bracketleft:    scancode      = KEY_LEFTBRACE;     break;
		case XK_bracketright:    scancode     = KEY_RIGHTBRACE;     break;
		case XK_Return:    scancode  = KEY_ENTER;     break;

		case XK_semicolon:    scancode        = KEY_SEMICOLON;     break;
		case XK_colon:    scancode    = KEY_SEMICOLON;     break;
		case XK_apostrophe:    scancode       = KEY_APOSTROPHE;     break;
		case XK_quotedbl:    scancode         = KEY_APOSTROPHE;     break;
		case XK_grave:    scancode    = KEY_GRAVE;     break;
		case XK_asciitilde:    scancode       = 0x2b;     break;
		case XK_backslash:    scancode        = KEY_BACKSLASH;     break;
		case XK_bar:    scancode              = 0x56;     break;  // "|" char

		case XK_comma:    scancode    = KEY_COMMA;      break;
		case XK_less:    scancode     = KEY_COMMA;      break;
		case XK_period:    scancode   = KEY_DOT;      break;
		case XK_greater:    scancode  = KEY_DOT;      break;
		case XK_slash:    scancode    = KEY_SLASH;      break;
		case XK_question:    scancode         = KEY_SLASH;      break;
		case XK_Caps_Lock:    scancode        = KEY_CAPSLOCK;      break;

		case XK_F1:    scancode               = KEY_F1; break;
		case XK_F2:    scancode               = KEY_F2; break;
		case XK_F3:    scancode               = KEY_F3; break;
		case XK_F4:    scancode               = KEY_F4; break;
		case XK_F5:    scancode               = KEY_F5; break;
		case XK_F6:    scancode               = KEY_F6; break;
		case XK_F7:    scancode               = KEY_F7; break;
		case XK_F8:    scancode               = KEY_F8; break;
		case XK_F9:    scancode               = KEY_F9; break;
		case XK_F10:    scancode              = KEY_F10; break;
		case XK_Num_Lock:    scancode         = KEY_NUMLOCK; break;
		case XK_Scroll_Lock:    scancode      = KEY_SCROLLLOCK; break;

		case XK_Page_Down:    scancode        = KEY_PAGEDOWN; break;
		case XK_Insert:    scancode   = KEY_INSERT; break;
		case XK_Delete:    scancode   = KEY_DELETE; break;
		case XK_Page_Up:    scancode  = KEY_PAGEUP; break;
		case XK_Escape:    scancode   = KEY_ESC; break;

		case XK_KP_Divide:   scancode = KEY_KPSLASH; break;
		case XK_KP_Multiply: scancode = KEY_KPASTERISK; break;
		case XK_KP_Add:      scancode = KEY_KPPLUS; break;
		case XK_KP_Subtract: scancode = KEY_KPMINUS; break;
		case XK_KP_Enter:    scancode = KEY_KPENTER; break;

		case XK_KP_Decimal:
		case XK_KP_Delete:
			scancode = KEY_KPDOT; break;

		case XK_KP_0:
		case XK_KP_Insert:
			scancode = KEY_KP0; break;

		case XK_KP_1:
		case XK_KP_End:
			scancode = KEY_KP1; break;

		case XK_KP_2:
		case XK_KP_Down:
			scancode = KEY_KP2; break;

		case XK_KP_3:
		case XK_KP_Page_Down:
			scancode = KEY_KP3; break;

		case XK_KP_4:
		case XK_KP_Left:
			scancode = KEY_KP4; break;

		case XK_KP_5:
			scancode = KEY_KP5; break;

		case XK_KP_6:
		case XK_KP_Right:
			scancode = KEY_KP6; break;

		case XK_KP_7:
		case XK_KP_Home:
			scancode = KEY_KP7; break;

		case XK_KP_8:
		case XK_KP_Up:
			scancode = KEY_KP8; break;

		case XK_KP_9:
		case XK_KP_Page_Up:
			scancode = KEY_KP9; break;

		}
	}

	return scancode;
}


// Rotation logic
int mod(int a, int b)  // modulus/division-remainder
{ return (a%b+b)%b; }

void set_orientation_flags() {
    bool landscape_cp = landscape;
    switch (orientation) {
        case 0:
	    mirror = false;
	    landscape = false;
	    break;
	case 1:
	    mirror = false;
	    landscape = true;
	    break;
	case 2:
	    mirror = true;
	    landscape = false;
	    break;
	case 3:
	    mirror = true;
	    landscape = true;
	    break;
    }

    if (landscape != landscape_cp) {  // Landscape mode toggled
        std::swap(vnc_surface_width, vnc_surface_height);
    }
}

void rotate(int direction, rfbClientPtr cl) {
    orientation += direction;
    orientation = mod(orientation, 4);  // 4 orientations
    set_orientation_flags();
}


// vnc key event function, escape to quit
void dokey(rfbBool down,rfbKeySym key,rfbClientPtr cl)
{
  printf("keysym: %04X\n", key);
  keys[key] = down;

  if (down && keys[0xffe3] && keys[0xffeb] && key == rot_left)
      rotate(-1, cl);
  else if (down && keys[0xffe3] && keys[0xffeb] && key == rot_right)
      rotate(1, cl);
  else if (down && keys[0xffe3] && keys[0xffeb] && key == key_pause)
      paused ^= true;
  else
      libevdev_uinput_write_event(uidev, EV_KEY, keysym2scancode(key), down);
      libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
}


void doptr(int buttonMask, int x, int y, rfbClientPtr cl)
{
  int left_down = buttonMask & 0x1;
  int right_down = buttonMask & 0x4;
  //int middle_down = buttonMask & 0x2;
  //printf("BM: %d\n", buttonMask);

  if (mirror) {
    y = vnc_surface_height - y;
    x = vnc_surface_width - x;
  }

  if (landscape) {
    x = vnc_surface_width - x;
    std::swap(x, y);
  }

  if (left_down) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_LEFT, 1);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);

    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_TRACKING_ID, 10);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOUCH, 1);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOOL_FINGER, 1);
    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_POSITION_X, x*scale_factor);
    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_POSITION_Y, y*scale_factor);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    lmb = true;
  }
  else if (lmb) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_LEFT, 0);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);

    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_TRACKING_ID, -1);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOUCH, 0);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOOL_FINGER, 0);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    lmb = false;
  }

  if (right_down || rmb) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_RIGHT, right_down >> 2);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    rmb ^= true;
  }

  /**if (middle_down || mmb) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_MIDDLE, middle_down >> 1);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    mmb ^= true;
  }**/
}


void do_screencast(EGLSetup const& egl_setup,
                   mir::geometry::Size const& size,
                   int32_t number_of_captures,
                   double capture_fps,
                   std::ostream& out_stream)
{
    static int const rgba_pixel_size{4};
    auto const frame_size_bytes = rgba_pixel_size *
                                  size.width.as_uint32_t() *
                                  size.height.as_uint32_t();

    std::vector<char> frame_data(frame_size_bytes, 0);
    auto format = egl_setup.pixel_read_format();

    auto capture_period = std::chrono::duration<double>(1.0/capture_fps);

    // vnc server
    int bpp = 4;
    vnc_surface_width = size.width.as_uint32_t();
    vnc_surface_height = size.height.as_uint32_t();

    // update orientation
    set_orientation_flags();

    rfbScreenInfoPtr rfbScreen = rfbGetScreen(nullptr,nullptr,vnc_surface_width,vnc_surface_height,8,3,bpp);
    rfbScreen->desktopName = "MIR Screen";
    rfbScreen->frameBuffer = frame_data.data();
    rfbScreen->alwaysShared = TRUE;
    if (!viewonly) {
        rfbScreen->kbdAddEvent = dokey;
        rfbScreen->ptrAddEvent = doptr;
    }
    rfbInitServer(rfbScreen);
    rfbRunEventLoop(rfbScreen,-1,TRUE);
    fprintf(stderr, "Running vnc background loop...\n");

    std::vector<double> fps_arr;

    while (running)
    {
        auto time_point = std::chrono::steady_clock::now() + capture_period;

	    if (vnc_surface_width/vnc_surface_height != rfbScreen->width/rfbScreen->height) {
	        printf("Ratios don't match\n");
	        printf("RFB: %dx%dx%d\n", rfbScreen->width, rfbScreen->height, rfbScreen->paddedWidthInBytes);
	        printf("Screen: %dx%d\n",  size.width, size.height);

	        std::swap(rfbScreen->width, rfbScreen->height);
	        rfbScreen->paddedWidthInBytes = rfbScreen->width * bpp;

	        printf("RFB: %dx%dx%d\n\n", rfbScreen->width, rfbScreen->height, rfbScreen->paddedWidthInBytes);

	        {
		    rfbClientIteratorPtr iterator;
		    rfbClientPtr cl;
		    iterator = rfbGetClientIterator(rfbScreen);
		    while ((cl = rfbClientIteratorNext(iterator)) != nullptr)
			    cl->newFBSizePending = 1;
	        }
	        rfbMarkRectAsModified(rfbScreen, 0, 0, rfbScreen->width, rfbScreen->height);
	    }

        int w = rfbScreen->width;
        int h = rfbScreen->height;
        auto write_out_future = std::async(
	        std::launch::async,
	        [rfbScreen, w, h] {
	            if (running)
		        rfbMarkRectAsModified(rfbScreen, 0, 0, w, h);
	        }
	    );

        egl_setup.swap_buffers();
        write_out_future.wait();

	if (!paused)
            read_pixels(bpp, format, size, frame_data);

        std::this_thread::sleep_until(time_point);
    }
}


void create_input_device(MirScreencastParameters const& params) {
    // Fake input device (https://stackoverflow.com/q/23092855)
    dev = libevdev_new();
    libevdev_set_name(dev, "mirvncserver input device");
    libevdev_set_id_vendor(dev, 0x1);
    libevdev_set_id_product(dev, 0x1);
    libevdev_set_id_version(dev, 0x1);
    libevdev_set_id_bustype(dev, BUS_USB);

    // (Absolute) pointer input (http://who-t.blogspot.com/2013/09/libevdev-accessing-and-modifying-devices.html)
    struct input_absinfo abs;
    abs.minimum = abs.resolution = abs.fuzz = abs.flat = 0;

    libevdev_enable_event_type(dev, EV_ABS);

    abs.maximum = 9;
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_SLOT, &abs);

    abs.maximum = 65535;
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_TRACKING_ID, &abs);

    abs.maximum = params.region.width;
    libevdev_enable_event_code(dev, EV_ABS, ABS_X, &abs);
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_POSITION_X, &abs);

    abs.maximum = params.region.height;
    libevdev_enable_event_code(dev, EV_ABS, ABS_Y, &abs);
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_POSITION_Y, &abs);

    // Keyboard input
    libevdev_enable_event_type(dev, EV_KEY);
    libevdev_enable_event_code(dev, EV_KEY, BTN_LEFT, nullptr);
    libevdev_enable_event_code(dev, EV_KEY, BTN_RIGHT, nullptr);
    libevdev_enable_event_code(dev, EV_KEY, BTN_MIDDLE, nullptr);
    libevdev_enable_event_code(dev, EV_KEY, BTN_TOUCH, nullptr);
    libevdev_enable_event_code(dev, EV_KEY, BTN_TOOL_FINGER, nullptr);
    for (int i = 0; i < KEY_MAX; i++)
      libevdev_enable_event_code(dev, EV_KEY, i, nullptr);

    libevdev_uinput_create_from_device(dev,
        LIBEVDEV_UINPUT_OPEN_MANAGED,
        &uidev);
}


int main(int argc, char* argv[])
try
{
    uint32_t output_id = mir_display_output_id_invalid;
    int number_of_captures = -1;
    std::string output_filename;
    std::string socket_filename;
    std::vector<int> screen_region;
    std::vector<int> requested_size;
    bool use_std_out = false;
    bool query_params_only = false;
    int capture_interval = 1;

    //avoid unused warning/error
    dummy_tls[0] = 0;

    po::options_description desc("Usage");
    desc.add_options()
        ("help,h", "displays this message")
	("view-only", "Disable interaction")
        ("orientation,o",
            po::value<int>(&orientation), "orientation of image: 4 options, each a 90 degree rotation")
        ("number-of-frames,n",
            po::value<int>(&number_of_captures), "number of frames to capture")
        ("display-id,d",
            po::value<uint32_t>(&output_id), "id of the display to capture")
        ("mir-socket-file,m",
            po::value<std::string>(&socket_filename), "mir server socket filename")
        ("file,f",
            po::value<std::string>(&output_filename), "output filename (default is /tmp/mir_screencast_<w>x<h>.<rgba|bgra>")
        ("size,s",
            po::value<std::vector<int>>(&requested_size)->multitoken(),
            "screencast size [width height]")
        ("screen-region,r",
            po::value<std::vector<int>>(&screen_region)->multitoken(),
            "screen region to capture [left top width height]")
        ("stdout", po::value<bool>(&use_std_out)->zero_tokens(), "use stdout for output (--file is ignored)")
        ("query",
            po::value<bool>(&query_params_only)->zero_tokens(),
            "only queries the colorspace and output size used but does not start screencast")
        ("rotate_left", po::value<std::string>(), "Hex keysym for rotating left (default is Ctrl+Super+Up - 0xFF52)")
        ("rotate_right", po::value<std::string>(), "Hex keysym for rotating right (default is Ctrl+Super+Down - 0xFF54)")
        ("cap-interval",
            po::value<int>(&capture_interval),
            "adjusts the capture rate to <arg> display refresh intervals\n"
            "1 -> capture at display rate\n2 -> capture at half the display rate, etc..");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("size") && requested_size.size() != 2)
            throw po::error("invalid number of parameters specified for size");

        if (vm.count("screen-region") && screen_region.size() != 4)
            throw po::error("invalid number of parameters specified for screen-region");

        if (vm.count("display-id") && vm.count("screen-region"))
            throw po::error("cannot set both display-id and screen-region");

        if (vm.count("cap-interval") && capture_interval < 1)
            throw po::error("invalid capture interval");

	if (vm.count("rotate_left")) {
	    std::stringstream interpreter;
            interpreter << std::hex << vm["rotate_left"].as<std::string>();
	    interpreter >> rot_left;
	}

	if (vm.count("rotate_right")) {
	    std::stringstream interpreter;
            interpreter << std::hex << vm["rotate_right"].as<std::string>();
	    interpreter >> rot_right;
	}

	if (vm.count("orientation")) {
	    orientation = vm["orientation"].as<int>();
	}

	if (vm.count("view-only")) {
	    viewonly = true;
	}
    }
    catch(po::error& e)
    {
        std::cerr << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    signal(SIGINT, shutdown);
    signal(SIGTERM, shutdown);

    char const* socket_name = vm.count("mir-socket-file") ? socket_filename.c_str() : nullptr;
    auto const connection = mir::raii::deleter_for(
        mir_connect_sync(socket_name, "mirscreencast"),
        [](MirConnection* c) { if (c) mir_connection_release(c); });

    if (connection == nullptr || !mir_connection_is_valid(connection.get()))
    {
        std::string msg("Failed to connect to server.");
        if (connection)
        {
            msg += " Error was :";
            msg += mir_connection_get_error_message(connection.get());
        }
        throw std::runtime_error(std::string(msg));
    }

    auto const display_config = mir::raii::deleter_for(
        mir_connection_create_display_config(connection.get()),
        &mir_display_config_destroy);

    if (display_config == nullptr)
        throw std::runtime_error("Failed to get display configuration\n");

    MirScreencastParameters params =
        get_screencast_params(connection.get(), *display_config, requested_size, screen_region, output_id);

    // Create input device
    create_input_device(params);

    // Calculate scale factor (FIXME: assumes scale ratio is equivalent on both axes)
    scale_factor = (float)params.region.width / (float)params.width;
    printf("%d %d %f\n", params.width, params.region.width, scale_factor);

    double capture_rate_limit = get_capture_rate_limit(*display_config, params);
    double capture_fps = capture_rate_limit/capture_interval;

    auto const screencast = mir::raii::deleter_for(
        mir_connection_create_screencast_sync(connection.get(), &params),
        [](MirScreencast* s) { if (s) mir_screencast_release_sync(s); });

    if (screencast == nullptr)
        throw std::runtime_error("Failed to create screencast");

    EGLSetup egl_setup{connection.get(), screencast.get()};
    mir::geometry::Size screencast_size {params.width, params.height};

    if (output_filename.empty() && !use_std_out)
    {
        std::stringstream ss;
        ss << "/tmp/mir_screencast_" ;
        ss << screencast_size.width << "x" << screencast_size.height;
        ss << "_" << capture_fps << "Hz";
        ss << (egl_setup.pixel_read_format() == GL_BGRA_EXT ? ".bgra" : ".rgba");
        output_filename = ss.str();
    }

    if (query_params_only)
    {
       std::cout << "Colorspace: " <<
           (egl_setup.pixel_read_format() == GL_BGRA_EXT ? "BGRA" : "RGBA") << std::endl;
       std::cout << "Output size: " <<
           screencast_size.width << "x" << screencast_size.height << std::endl;
       std::cout << "Capture rate (Hz): " << capture_fps << std::endl;
       std::cout << "Output to: " <<
           (use_std_out ? "standard out" : output_filename) << std::endl;
       return EXIT_SUCCESS;
    }

    if (use_std_out)
    {
        do_screencast(egl_setup, screencast_size, number_of_captures, capture_fps, std::cout);
    }
    else
    {
        std::ofstream file_stream(output_filename);
        do_screencast(egl_setup, screencast_size, number_of_captures, capture_fps, file_stream);
    }

    // Clear libevdev devices
    libevdev_uinput_destroy(uidev);
    libevdev_free(dev);

    return EXIT_SUCCESS;
}
catch(std::exception const& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
