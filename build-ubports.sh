#!/bin/bash

echo "${ARCH_TRIPLET}"
ls /usr/include/*/qt5

cd "${ROOT}" || exit
make --always-make -j$(( ( $(getconf _NPROCESSORS_ONLN) + 1) / 2 )) MULTIARCH="${ARCH_TRIPLET}"

rm -f vncserver.sh
cat << EOF >> vncserver.sh
#!/bin/sh
SCRIPTPATH="\$(dirname \$(realpath -s \$0))"
export LD_LIBRARY_PATH="\$SCRIPTPATH/lib/${ARCH_TRIPLET}/"

\$SCRIPTPATH/mirvncserver "\$@"
EOF

chmod +x vncserver.sh
